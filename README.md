# punk-beer-client
A basic demo utilising:
1. An accordion that lists beer names, that expand to show tagline and date first brewed.
2. A full width CTA grid of at least 8 beers with sorts for name and ABV.

The site is available at: https://pedantic-colden-ef1b79.netlify.app/
## Project setup
### This project uses Node version 12.19.0 and Vue CLI version 4
```

npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

## Enhancements (TODO)
1. Currently the CTA grid shows the top eight beers, 
  a. this could be made selective, so the user can select how many they want to see.
  b. the number shown could depend on the width of the viewport, which would improve the look on larger screens
  29/08/2021 - Replaced buttons for sorting with a dropdown list as this is a more common control

2. Styling can be vastly improved on and for a larger application SCSS would be used. I have kept it simple as styling is not my forte
  29/08/2021 - Some basic styling changes made to help with usability

3. The accordian panels need transitioning added, so they glide
  29/08/2021 - transitioning added

4. API call needs to be moved to a separate file

5. Tests

4. Code clean-up and review
